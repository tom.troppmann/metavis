#pragma once
#include <random>
class PseudoRandom
{
public:
	PseudoRandom();
	bool randomBool();
	double doubleRandom();
	int randomIntegerInRange(int min, int max);
private:
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 generator; //Standard mersenne_twister_engine seeded with rd()
	std::bernoulli_distribution boolDistribution;
	std::uniform_real_distribution<double> doubleDistribution;
};

