#pragma once
#include<vector>
#include<string>



struct Solution {
    std::vector<bool> bitstring;
    double objectiveFunctionValue;
    Solution();
    Solution(std::vector<bool> bitstring, double objectiveFunctionValue);
    std::string bitstringToStdString();
};
namespace objective {
	double onemaxLinear(const std::vector<bool>& bitstring);
    double onemaxWithTrap(const std::vector<bool>& bitstring);
    double onemaxTrigger(const std::vector<bool>& bitstring);
    class OneMax{
    public:
        std::vector<int> results;
        OneMax(int n, int k);
        void generateKStep(int k);
        double function(const std::vector<bool>&);
    };
    // 
    struct Item {
        double value;
        double weight;
    };
    class KnapsackProblem {
    public:
        KnapsackProblem(double weightLimit,double limitPenalty);
        // List of items that can be selected
        std::vector<Item> itemVec;
        // Limit of the maximum weight that will apply the penalty
        double weightLimit;
        // The penalty for exceeding the limit
        double limitPenalty;
        double function(const std::vector<bool>&);
    };
}

