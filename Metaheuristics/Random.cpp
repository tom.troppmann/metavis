#include "Random.h"

PseudoRandom::PseudoRandom(): generator(rd()), boolDistribution(0.5), doubleDistribution(0,1)
{
}

bool PseudoRandom::randomBool()
{
	return boolDistribution(generator);
}

double PseudoRandom::doubleRandom()
{
	return doubleDistribution(generator);
}

int PseudoRandom::randomIntegerInRange(int min, int max)
{
	return std::uniform_int_distribution(min,max-1)(generator);
}
