#pragma once
#include "ObjectiveFunction.h"
#include "BinaryHeuristic.h"
#include <ostream>


class GeneticAlgorithm : BinaryHeuristic
{
public:
	GeneticAlgorithm(std::ostream& outstream, int iteration, int population, double mutateProbability = 0.01, double swapProbability = 0.5, double tournamentSize = 2.0);
	Solution execute(int round, int n, std::function<double(const std::vector<bool>&)> objectiveFunction, ObjectiveFunctionGoal goal) override;
protected:
private:
	//params:
	int iteration;
	int populationSize;
	double mutateProbability;
	double swapProbability;
	double tournamentSize;
	//methods:
	Solution& selectAParent(std::vector<Solution>& population, std::function<bool(double, double)> op);
	void crossover(Solution& childA, Solution& childB);
	void mutate(Solution& individual);
};

