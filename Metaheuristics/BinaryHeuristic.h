#pragma once
#include "ObjectiveFunction.h"
#include <functional>
#include <ostream>
#include "Random.h"

class BinaryHeuristic
{
public:
	BinaryHeuristic(std::ostream& outstream);
	enum class ObjectiveFunctionGoal { min, max };
	virtual Solution execute(int rounds, int n, std::function<double(const std::vector<bool>&)> objectiveFunction, ObjectiveFunctionGoal goal) = 0;
protected:
	//Interfaces
	PseudoRandom rand;
	std::ostream& outstream;
	std::function<bool(double, double)> getOperatorFromGoal(ObjectiveFunctionGoal goal);
private:
	
};

