#pragma once
#include "ObjectiveFunction.h"
#include "BinaryHeuristic.h"
#include "Random.h"
#include <ostream>
#include <functional>

class BinaryAntColonyOptimization;
typedef BinaryAntColonyOptimization Baco;





class BinaryAntColonyOptimization : BinaryHeuristic
{
public:
	BinaryAntColonyOptimization(std::ostream& outstream, int iteration, int population, double vaporization = 0.3, double resetThreshold = 0.99);
	Solution execute(int round, int n, std::function<double(const std::vector<bool>&)> objectiveFunction, ObjectiveFunctionGoal goal) override;
protected:

private:
	//Params:
	int maxIteration;
	int amountOfPopulation;
	double vaporization;
	double resetThreshold;
	//Methods 
	double calculateConvergenceFactor(std::vector<double> pheromons);
};

