#include "BinaryHeuristic.h"

BinaryHeuristic::BinaryHeuristic(std::ostream& outstream):
outstream(outstream)
{

}

std::function<bool(double, double)> BinaryHeuristic::getOperatorFromGoal(ObjectiveFunctionGoal goal)
{
	switch (goal) {
	case ObjectiveFunctionGoal::min:
		return std::less<double>();
	case ObjectiveFunctionGoal::max:
	default:
		return std::greater<double>();
	}
}
