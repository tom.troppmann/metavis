#pragma once

#include <QWidget>
#include "ui_tsneIteractive.h"
#include "GraphView.h"
#include "RunData.h"
#include "ColorGradient.h"
#include "tSneAlgo.h"

class tsneIteractive : public QWidget
{
	Q_OBJECT
public:
	tsneIteractive(QWidget *parent = Q_NULLPTR);
	~tsneIteractive();

	GraphView* view;
	ColorGradient* gradient;
	RunData* data = nullptr;
	void assignRunData(RunData* data);
	tSneAlgo* tsneConcurrent = nullptr;
	tSneAlgo* getTsneConcurrent();
private:
	Ui::tsneIteractive ui;
	
	//Concurrent
	QTimer* timer = new QTimer(this);
	void updateCanvasIfAlgoRuns();
	void updateViewGradient();


public slots:
	void startRun();
	void pauseRun();
};
