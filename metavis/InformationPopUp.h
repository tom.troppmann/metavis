#pragma once

#include <QWidget>
#include <QString>
#include <QLabel>
class InformationPopUp : public QWidget
{
	Q_OBJECT

public:
	InformationPopUp(QWidget *parent);
	~InformationPopUp();
	void setInformation(QString information);
	QString getInformation() const;
private:
	QLabel* informationLabel = new QLabel();
};
