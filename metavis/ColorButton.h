#pragma once

#include <QPushButton>
#include <QColor>
class ColorButton : public QPushButton
{
	Q_OBJECT


public:
	ColorButton(QWidget *parent, QColor color);
	~ColorButton();
	QColor getColor();
private:
	QColor color;
	void updateVisual();
	void resizeEvent(QResizeEvent* event) override;
	void openColorMenu();
public slots:
	void setColor(QColor color);
signals:
	void colorChanged(QColor color);
};
