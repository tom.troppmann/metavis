#pragma once
//QT
#include <QWidget>
#include <QPaintEvent>
#include <QPen>
#include <QPainter>
#include <QSettings>
#include <QPoint>
#include <QDialog>
#include <QAbstractButton>
#include <QDebug>
#include <QBrush>
#include <QtWidgets/QApplication>
#include <QtGui>
#include <QStandardPaths>
#include <QDockwidget>
#include <QLabel>
#include <QLayout>
#include <QStyleFactory>
#include <QFileDialog>
#include <QDir>
#include <QString>
#include <QToolTip>
#include <QTimer>
#include <QSlider>

//Std
#include <list>
#include <vector>
#include <string>
#include <fstream>
#include <map>
#include <random>
#include <algorithm>
#include <sstream>
#include <regex>