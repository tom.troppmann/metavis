#include "pch.h"
#include "ColorButton.h"
#include <QColorDialog>
#include <QPixmap>

ColorButton::ColorButton(QWidget *parent, QColor color)
	: QPushButton(parent), color(color)
{
	//setText("Set Color");
	
	//setContentsMargins(0, 0, 0, 0);
	//this->setStyleSheet(this->styleSheet() + "border: 1px solid white; background-color: red");
	updateVisual();
	setToolTip("Set Color");
	connect(this, &QPushButton::pressed, this, &ColorButton::openColorMenu);
}

ColorButton::~ColorButton()
{
}

QColor ColorButton::getColor()
{
	return color;
}

void ColorButton::updateVisual()
{
	QPixmap pixmap(100, 14);
	pixmap.fill(color);
	QIcon redIcon(pixmap);
	setIcon(QIcon(redIcon));
	setIconSize(QSize(100, 14));
}

void ColorButton::resizeEvent(QResizeEvent* event)
{
}

void ColorButton::openColorMenu()
{
	QColor color = QColorDialog::getColor(this->color, this, "Set Color", QColorDialog::ShowAlphaChannel);
	if (!color.isValid()) return;
	this->color = color;
	updateVisual();
	emit colorChanged(this->color);
}

void ColorButton::setColor(QColor color)
{
	this->color = color;
	updateVisual();
}




