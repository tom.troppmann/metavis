#pragma once

#include <QWidget>
#include <SearchSpacePlott.h>
#include <vector>
#include "RunData.h"
#include "ColorGradient.h"


class TsnePlott : public SearchSpacePlott
{
	Q_OBJECT



public:
	TsnePlott(QWidget *parent);
	~TsnePlott();
	void assignMatrix(std::vector<SolutionPointData>::iterator begin, std::vector<SolutionPointData>::iterator end,  double* yMatrixFromTsneAlgo, int n, ColorGradient& gradient);
	void clear();
	void updateColors(ColorGradient& gradient);
	void virtual frameGraphInView();
protected:
	virtual void drawData(QPainter& painter) override;
	virtual void searchForPointUnderCursor() override;
	virtual void addPointsInWindowToScratchPad(VisibleWindow& window) override;
private:
	std::vector<SolutionPointData>::iterator begin, end;
	std::vector<QColor> colorPointDataVec;
	double* yMatrixFromTsneAlgo = nullptr;
	int N;

};
