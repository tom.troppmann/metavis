#pragma once

#include <QWidget>
#include <QLineEdit>
#include <QSlider>
#include <QGridLayout>

class TsneSettings : public QWidget
{
	Q_OBJECT

public:
	TsneSettings(QWidget *parent);
	~TsneSettings();
	int maxIter;
	int perplexity, perplexityMax, perplexityMin;
	int learnrate, learnrateMax, learnrateMin;
	void setPerplexity(int value);
	void setLearnrate(int value);
protected:
	QGridLayout* tsneSettings = new QGridLayout(this);
	QSlider* perplexitySlider = new QSlider(Qt::Orientation::Horizontal);
	QSlider* learnrateSlider = new QSlider(Qt::Orientation::Horizontal);
	QLineEdit* perplexityEdit = new QLineEdit();
	QLineEdit* learnrateEdit = new QLineEdit();
private:
	void readSettigns();
	void writeSettings();
	void learnrateEditChangeEvent();
	void perplexityEditChangeEvent();
signals:
	void perplexityChanged(int perplexity);
	void learnrateChanged(int learnrate);
};
