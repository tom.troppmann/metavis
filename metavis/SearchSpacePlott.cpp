#include "pch.h"
#include "SearchSpacePlott.h"

SearchSpacePlott::SearchSpacePlott(QWidget *parent)
	: GraphPlott(parent, false, true, false)
{
	QSlider* circleSizeSlider = new QSlider(Qt::Horizontal);
	circleSizeSlider->setMinimum(1);
	circleSizeSlider->setMaximum(15);
	circleSizeSlider->setValue(circleSize);
	circleSizeSlider->setMaximumWidth(80);
	circleSizeSlider->setMaximumHeight(16);
	this->buttonPanel->insertSpacing(1, 5);
	this->buttonPanel->insertWidget(1, circleSizeSlider);
	this->buttonPanel->insertSpacing(1, 5);
	this->buttonPanel->insertWidget(1, new QLabel("Size:"));
	connect(circleSizeSlider, &QSlider::valueChanged, this, [this, circleSizeSlider]() {this->circleSize = circleSizeSlider->value(); this->update(); });
	QSlider* transparentSlider = new QSlider(Qt::Horizontal);
	transparentSlider->setMinimum(5);
	transparentSlider->setMaximum(50);
	transparentSlider->setValue(50);
	transparentSlider->setMaximumWidth(80);
	transparentSlider->setMaximumHeight(16);
	this->buttonPanel->insertSpacing(1, 5);
	this->buttonPanel->insertWidget(1, transparentSlider);
	this->buttonPanel->insertSpacing(1, 5);
	this->buttonPanel->insertWidget(1, new QLabel("Transparency:"));
	connect(transparentSlider, &QSlider::valueChanged, this, [this, transparentSlider]() {this->transparentAlphaValue = ((double)transparentSlider->value()) / 50.0; this->update(); });
	this->setMinimumSize(200, 50);
}

SearchSpacePlott::~SearchSpacePlott()
{
}

void SearchSpacePlott::mouseMoveEvent(QMouseEvent* event)
{
	GraphPlott::mouseMoveEvent(event);
}

void SearchSpacePlott::setMinimumIterationToDispaly(int min)
{
	minIter = min;
	update();
}

void SearchSpacePlott::setMaximumIterationToDispaly(int max)
{
	maxIter = max;
	update();
}
