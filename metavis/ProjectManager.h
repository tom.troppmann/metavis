#pragma once
#include <QTreeWidget>
#include <QWidget>
#include <list>
#include "metavis.h"
#include <future>
#include <QStringList>
#include <QFuture>
#include <QFutureWatcher>
#include "Project.h"
class metavis;

class ProjectManager : public QWidget
{
	Q_OBJECT

public:
	ProjectManager(metavis* owner);
	~ProjectManager();


	Project project;
	void openFileDialog();
	void openFiles(QStringList pathlist);
	QMenu* projectMenu;
private:
	std::future<void> loadingFuture = std::async(std::launch::async, []() {});
	QFuture<void> future;
	QFutureWatcher<void> watcher = QFutureWatcher<void>(this);
	metavis* owner;
	QTreeWidget* treeWidget;
	QTreeWidgetItem* projectItem;
	void displayNewFile();
	QMenu* metalogFileMenu;
	QMenu* singleRunMenu;
	QAction* removeMetalogFileAction;
	QAction* selectAction;
	enum Types{ LogManger = 1001, LogFile, Run, GraphManger, Graph, ProjectHeader};
	QString lastPath;
	void createTreeWidget();
	void showItem(QTreeWidgetItem* item);
private slots:
	void prepareMenu(const QPoint& pos);
	void itemClicked(QTreeWidgetItem* item, int column);
};
