#pragma once

#include <QPushButton>
#include <QPalette>
#include <QColor>
#include <QDebug>
#include <QIcon>

class HoverButton : public QPushButton
{
	Q_OBJECT

public:
	HoverButton(QWidget *parent = nullptr);
	~HoverButton();
	void setHoveredIcon(const QIcon& icon);

protected:
    virtual void enterEvent(QEvent*);
	virtual void leaveEvent(QEvent*);
private:
	QIcon normalIcon;
	QIcon hoveredIcon;
};
