#pragma once
#include <QtWidgets/QMainWindow>
#include <QSettings>
#include <vector>
#include "ui_metavis.h"
#include "RunData.h"
#include "GraphView.h"

#include "BitfieldControlPanel.h"
#include "TsneControlPanel.h"

#include "GraphPlott.h"
#include "BitInspectorPanel.h"
#include "Scratchpad.h"
#include "tsneIteractive.h"
#include "ProjectManager.h"
class ProjectManager;


/**
 * Main class of the GUI.
 */
class metavis : public QMainWindow
{
	Q_OBJECT

public:
	metavis(QWidget *parent = Q_NULLPTR);
	~metavis();
	GraphPlott* selectedBestAverageGraph;
	GraphPlott* selectedParticleGraph;
	GraphPlott* selectedMinMaxGraph;
	GraphPlott* selectedMeanHammingDistanceGraph;
	GraphPlott* selectedBestGraph;



	GraphPlott* multiBestGraph;
	GraphPlott* multiAvgGraph;
	GraphPlott* multiMinGraph;
	GraphPlott* multiMaxGraph;
	GraphPlott* multiMeanHammingDistanceGraph;
	
	GraphPlott* plottTest;
	
	BitfieldControlPanel* bitfieldPanel;
	BitInspectorPanel* inspectorPanel;
	Scratchpad* pad = new Scratchpad(this);
	tsneIteractive* tsneWidget;
	TsneControlPanel* tsnePanel;
	GraphView* bitField;

	enum dockOption{left, right, bottom, top, splitLeft, splitRight, splitBottom, splitTop, tab};

	void selectRunData(RunData* data);
	void selectSingleRun(SingleRun* run);
	void removeRunDataFromAllViews(RunData* data);





private:
	Ui::metavisClass ui;
	QSettings* settings;
	ProjectManager* manager;

	//To Tab all 
	QDockWidget* lastDocked;
	dockOption option;
	void dockWidget(QDockWidget* dock);

private:
	/* Widget functions */
	GraphView* createCustomWidget(QString titleString, bool tabToLast = false);
	void createBitFieldV2();
	void createProjectManager();
	void createTSNE(QDockWidget* dockto);
	GraphPlott* initGraphPlott(QString title, QString YAxisLegend = "ObjectiveFunction", QString XAxisLegend = "Iteration");
	void initPlotter();
	void createBitInSpector();
	void createScratchpad(QDockWidget* dockto);
	/* Setting functions*/
	void writeActualMainWindowSettings();
	void readMainWindowSettings();




public slots:
	/**
	 * Opens the settingWindows Dialog.
	 */
	void openSetting();
	/**
	 * Open a logFile.	
	 */
	void openFile();
	void showStatusBarLoading();
	void showStatusBarMessage(QString message);
	void clearStatusBar();
};
