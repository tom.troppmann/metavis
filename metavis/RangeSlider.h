#pragma once
#include <QColor>
#include <QWidget>
#include <QString>
#include <QLabel>



class RangeSlider : public QWidget
{
	Q_OBJECT

	
public:
	enum DisplayMode{ Range, Iteration};
	RangeSlider(QWidget *parent);
	~RangeSlider();
	void setMinSliderValue(int value);
	void setMaxSliderValue(int value);
	void setItertionSliderValue(int value);
	void setRange(int min, int max);
	int getMinRange() const;
	int getMaxRange() const;
	void setMinRange(int value);
	void setMaxRange(int value);
	void setMode(DisplayMode mode);
	void setTitle(QString string);
	//Visual Settings:
	void setRangeColor(QColor color);
	void setSliderRangeColor(QColor color);
private:
	int minRange = 0, maxRange = 100, minSliderValue = 0, maxSliderValue = 100, itertionSliderValue = 50;
	QColor rangeColor, sliderRangeColor;
	DisplayMode mode = DisplayMode::Range;
	void paintEvent(QPaintEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void updateSliderPosition(QMouseEvent* event);
	QLabel* titleLabel;
	int maxPos();
	int minPos();

signals:
	void maxChanged(int max);
	void minChanged(int min);
};
