#pragma once

#include "BitInspectorPanel.h"
#include "RunData.h"
#include <QMenu>

class Scratchpad : public BitInspectorPanel
{
	Q_OBJECT

public:
	Scratchpad(QWidget *parent);
	~Scratchpad();
	void addPoint(const SolutionPointData& point);
	void clear();
protected:
	QMenu* contextMenu = new QMenu(this);
	void contextMenuEvent(QContextMenuEvent* event) override;
private:
	//Points to display
	std::vector<SolutionPointData> points;
};
