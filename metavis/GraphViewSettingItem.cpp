#include "pch.h"
#include "GraphViewSettingItem.h"
#include <QColorDialog>

GraphViewSettingItem::GraphViewSettingItem(GraphPlott* view, GraphPlottSeries* series, QWidget *parent) : QWidget(parent), series(series), view(view), colorButton(new ColorButton(this, series->color))
{
	ui.setupUi(this);
	// Init
	switch (series->type) {
	case GraphPlottSeries::SeriesType::Dot:
		ui.SytleComboBox->setCurrentIndex(1);
		break;
	case GraphPlottSeries::SeriesType::LineDot:
		ui.SytleComboBox->setCurrentIndex(2);
		break;
	case GraphPlottSeries::SeriesType::Line:
	default:
		ui.SytleComboBox->setCurrentIndex(0);
		break;
	}
	ui.LineSlider->setValue(series->lineWidth);
	ui.CircleSlider->setValue(series->circleRadius);
	dissableSlidersOnType();
	updateGroupBoxTitle();
	ui.RightFormLayout->setWidget(1, QFormLayout::FieldRole, colorButton);

	
	//Connect
	connect(ui.LineSlider, &QSlider::valueChanged, [=](const int& newValue) { series->lineWidth = newValue; view->update(); });
	connect(ui.CircleSlider, &QSlider::valueChanged, [=](const int& newValue) { series->circleRadius = newValue; view->update(); });
	connect(ui.SytleComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [=](const int& newValue) { setType(newValue);dissableSlidersOnType();view->update(); });
	connect(ui.HideCheckbox, &QCheckBox::stateChanged, [=](const int& newValue) { series->hide = (newValue==2); view->update(); });
	//connect(ui.SetColorButton, &QPushButton::pressed, this, &GraphViewSettingItem::setColor);
	connect(colorButton, &ColorButton::colorChanged, this, &GraphViewSettingItem::setColor);
	connect(ui.NameEdit, &QLineEdit::textChanged, [=](const QString& text) { series->description = text; updateGroupBoxTitle(); });
}

GraphViewSettingItem::~GraphViewSettingItem()
{
}

void GraphViewSettingItem::dissableSlidersOnType()
{
	switch (series->type) {
	case GraphPlottSeries::SeriesType::Line:
		ui.LineSlider->setEnabled(true);
		ui.CircleSlider->setEnabled(false);
		break;
	case GraphPlottSeries::SeriesType::Dot:
		ui.LineSlider->setEnabled(false);
		ui.CircleSlider->setEnabled(true);
		break;
	case GraphPlottSeries::SeriesType::LineDot:
		ui.LineSlider->setEnabled(true);
		ui.CircleSlider->setEnabled(true);
		break;
	default:
		break;
	}
}

void GraphViewSettingItem::setType(int type)
{
	switch (type) {
	case 0:
		series->type = GraphPlottSeries::SeriesType::Line;
		break;
	case 1:
		series->type = GraphPlottSeries::SeriesType::Dot;
		break;
	case 2:
		series->type = GraphPlottSeries::SeriesType::LineDot;
		break;
	default:
		break;
	}
}

void GraphViewSettingItem::setColor(QColor color)
{
	series->color = color;
	updateGroupBoxTitle();
	view->update();
}

void GraphViewSettingItem::updateGroupBoxTitle()
{
	ui.GroupBox->setTitle("Series: " + series->description);
	QString style = "QGroupBox { color: rgb(%1, %2, %3); };";
	ui.GroupBox->setStyleSheet(style.arg(series->color.red()).arg(series->color.green()).arg(series->color.blue()));
}
