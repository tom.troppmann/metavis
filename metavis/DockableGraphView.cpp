#include "pch.h"
#include "DockableGraphView.h"
#include <QIcon>
#include <QStyle>
#include <QPushButton>
#include <QLayout>
#include <QGridLayout>


DockableGraphView::DockableGraphView(QWidget *parent, QString title)
	: QDockWidget(parent)
{
	view = new GraphView(this, Bound(0, 100, -1.0, 101.0));
	view->setUseFixedBound(true);
	view->setMinimumSize(200, 200);
	//dialog = new GraphViewSettingDialog(view, title + " Settings", this);
	QWidget* widget = new QWidget(this);
	QGridLayout* layout = new QGridLayout;
	QPushButton* button = new QPushButton();
	button->setMinimumSize(20, 20);
	button->setMaximumSize(20, 20);
	button->setIcon(QIcon(":/metavis/Resources/settingIcon.svg"));
	button->setAttribute(Qt::WA_TranslucentBackground);
	button->setStyleSheet(button->styleSheet() + "border: none;");
	button->setToolTip("Settings");
	
	//connect(button, &QPushButton::released, dialog, &GraphViewSettingDialog::openDialog);
	layout->addWidget(view, 0, 0, 4, 4);
	layout->addWidget(button,0, 3, 1, 1);
	layout->setContentsMargins(0, 0, 0, 0);
	widget->setLayout(layout);
	this->setWindowTitle(title);
	this->setAllowedAreas(Qt::AllDockWidgetAreas);
	this->setWidget(widget);
}

DockableGraphView::~DockableGraphView()
{
}
