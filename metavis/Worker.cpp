#include "pch.h"
#include "Worker.h"

Worker::Worker(std::thread t) : t(std::move(t))
{
}

Worker::~Worker()
{
	t.join();
}
