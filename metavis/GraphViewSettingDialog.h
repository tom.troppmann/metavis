#pragma once

#include <QDialog>
#include "GraphPlott.h"
#include "ui_GraphViewSettingDialog.h"
class GraphPlott;


class GraphViewSettingDialog : public QDialog
{
	Q_OBJECT

public:
	GraphPlott* view;
	GraphViewSettingDialog(GraphPlott* view, QString title);
	~GraphViewSettingDialog();
public slots:
	void openDialog();
private:
	int vecsize = 0;
	Ui::GraphViewSettingDialog ui;
	QVBoxLayout* layout;
};
