#pragma once
#include <QWidget>
#include <QPushButton>
#include "Bitfield.h"
#include "RangeSlider.h"
#include "ColorGradient.h"
#include "RunData.h"


class BitfieldControlPanel : public QWidget
{
	Q_OBJECT

public:
	BitfieldControlPanel(QWidget *parent);
	~BitfieldControlPanel();
	void updateDotColorsFromDisplayedRun();
	void displaySingleRun(SingleRun* run);
	void clearRun();
	Bitfield* field = new Bitfield(this);
protected:
	RangeSlider* slider = new RangeSlider(this);
	ColorGradient* gradient = new ColorGradient(this);
	bool isOptionHidden = false;
	void toggleOptions();
private:
	QPushButton* showOptionsButton;
	SingleRun* displayedRun = nullptr;
};
