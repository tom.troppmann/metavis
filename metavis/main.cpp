#include "pch.h"
#include "metavis.h"
#include <QtWidgets/QApplication>
#include <QtGui>
#include <QDebug>
#include <Vector>
#include <boost/multiprecision/cpp_int.hpp>
#include "RunData.h"
/* entrance of the program */
int main(int argc, char *argv[])
{ 
	QApplication a(argc, argv);
	metavis w;
	w.show();
	return a.exec();
}
