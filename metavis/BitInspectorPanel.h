#pragma once

#include <QWidget>
#include "BitInspector.h"
#include <string>
#include "HoverButton.h"
#include "InformationPopUp.h"
class BitInspectorPanel : public QWidget
{
	Q_OBJECT

public:
	BitInspectorPanel(QWidget *parent);
	~BitInspectorPanel();
	BitInspector* inspector;
	QLabel* runNameLabel;
	void setRunName(std::string& string);
	void removeRun();
	void showInformation();
protected:
	HoverButton* informationButton = new HoverButton();
	InformationPopUp* popupWidget = new InformationPopUp(nullptr);
	QHBoxLayout* topPanel;
};
