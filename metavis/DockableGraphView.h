#pragma once
#include <QDockWidget>
#include <QString>
#include "GraphView.h"
#include "GraphViewSettingDialog.h"
class DockableGraphView : public QDockWidget
{
	Q_OBJECT
public:
	GraphView* view;
	//GraphViewSettingDialog* dialog = nullptr;
public:
	DockableGraphView(QWidget *parent, QString title);
	~DockableGraphView();
public:
};
