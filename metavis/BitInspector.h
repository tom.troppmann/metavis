#pragma once

#include <QWidget>
#include "RunData.h"
#include <QString>
#include <vector>



class BitInspector : public QWidget
{
	Q_OBJECT

public:
	std::vector<SolutionPointData>::iterator begin;
	std::vector<SolutionPointData>::iterator end;
	int sizePerBit = 3;

	QPixmap map;
	BitInspector(QWidget *parent);
	~BitInspector();
	void updateData(std::vector<SolutionPointData>::iterator begin, std::vector<SolutionPointData>::iterator end);
	void clear();
private:
	void keyPressEvent(QKeyEvent* event) override;
	void paintEvent(QPaintEvent* event) override;
};
