#include "pch.h"
#include "HoverButton.h"

HoverButton::HoverButton(QWidget *parent)
	: QPushButton(parent)
{
}

HoverButton::~HoverButton()
{
}

void HoverButton::setHoveredIcon(const QIcon& icon)
{
	hoveredIcon = icon;
}

void HoverButton::enterEvent(QEvent*)
{
	normalIcon = this->icon();
	this->setIcon(hoveredIcon);
}

void HoverButton::leaveEvent(QEvent*)
{
	this->setIcon(normalIcon);
}
