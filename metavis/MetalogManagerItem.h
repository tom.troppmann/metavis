#pragma once

#include <QWidget>
#include "ui_MetalogManagerItem.h"

class MetalogManagerItem : public QWidget
{
	Q_OBJECT

public:
	MetalogManagerItem(QWidget *parent = Q_NULLPTR);
	~MetalogManagerItem();

private:
	Ui::MetalogManagerItem ui;
};
