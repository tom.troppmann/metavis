#include "pch.h"
#include "BitInspectorPanel.h"
#include <QScrollArea>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSettings>
#include <QApplication>
#include <QDesktopWidget>

BitInspectorPanel::BitInspectorPanel(QWidget *parent)
	: QWidget(parent), inspector(new BitInspector(this))
{
	QScrollArea* scrollArea = new QScrollArea(this);
	scrollArea->setWidget(inspector);
	scrollArea->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
	scrollArea->setWidgetResizable(true);
	QVBoxLayout* layout = new QVBoxLayout(this);
	layout->setContentsMargins(0, 0, 0, 0);
	layout->setSpacing(0);
	topPanel = new QHBoxLayout();
	topPanel->setContentsMargins(0, 0, 0, 0);
	topPanel->setSpacing(0);
	topPanel->addWidget(new QLabel("Iteration"));
	topPanel->addSpacing(2);
	topPanel->addWidget(new QLabel("Number"));
	topPanel->addSpacing(2);
	topPanel->addWidget(new QLabel("ObjectiveFunction"));
	topPanel->addSpacing(2);
	topPanel->addWidget(new QLabel("Binary"));
	topPanel->addStretch();
	runNameLabel = new QLabel("");
	topPanel->addWidget(runNameLabel);

	layout->addLayout(topPanel);
	layout->addWidget(scrollArea);
	popupWidget->setInformation("<h3><u>Inspector</u></h3> Shows all solution of a round. Each solution gets displayed in a row.<br><br><b>Iteration:</b> Show the iteration the solution is generated.<br><b>Number:</b> Show which individual in the popullation is represent.<br><b>Objective Function:</b> Describes how well the solution is, it depends on the problem if a low or high value is desired.<br><b>Binary:</b> Shows the bitstring of the solution. White rectangle represents the bit is <i>false<i> and black <i>true<i>.");
	informationButton->setMinimumSize(20, 20);
	informationButton->setMaximumSize(20, 20);
	informationButton->setIcon(QIcon(":/metavis/Resources/information_icon.svg"));
	informationButton->setHoveredIcon(QIcon(":/metavis/Resources/information_icon_hovered.svg"));
	informationButton->setAttribute(Qt::WA_TranslucentBackground);
	informationButton->setStyleSheet(informationButton->styleSheet() + "border: none;");
	informationButton->setToolTip("Information");
	connect(informationButton, &QPushButton::released, this, &BitInspectorPanel::showInformation);
	topPanel->addWidget(informationButton);
}

BitInspectorPanel::~BitInspectorPanel()
{
}

void BitInspectorPanel::setRunName(std::string& string)
{
	runNameLabel->setText("\"" + QString::fromStdString(string) + "\"");
}

void BitInspectorPanel::removeRun()
{
	inspector->clear();
	runNameLabel->setText("");
}

void BitInspectorPanel::showInformation()
{
	QPoint position = QCursor::pos();
	popupWidget->width();
	QRect screen = QApplication::desktop()->screenGeometry();
	if (position.x() + popupWidget->width() > screen.width()) {
		popupWidget->move(screen.width() - popupWidget->width(), position.y());
	}
	else {
		popupWidget->move(position.x(), position.y());

	}
	popupWidget->show();

}