#include "pch.h"
#include "BitInspector.h"

#define ItemWidth 20
#define FieldStart 170
#define EmptySpaceRight 5
#define EmptySpaceBottom 40
#define HeaderSpace 0

BitInspector::BitInspector(QWidget* parent)
	: QWidget(parent)
{
	//DummyData
	//std::vector< SolutionPointData>* dummyVec = new std::vector< SolutionPointData>();
	//SolutionPointData data;
	//data.iteration = 20;
	//data.bitVec.push_back(true);
	//data.objectiveFunction = 230.234;
	//data.particleNumber = 120;
	//dummyVec->push_back(data);
	//begin = dummyVec->begin();
	//end = dummyVec->end();
}

BitInspector::~BitInspector()
{
	
}

void BitInspector::updateData(std::vector<SolutionPointData>::iterator begin, std::vector<SolutionPointData>::iterator end)
{
	this->begin = begin;
	this->end = end;
	setMinimumSize(FieldStart + begin->bitVec.size() * 1 + EmptySpaceRight, std::distance(begin, end) * ItemWidth + EmptySpaceBottom + HeaderSpace);
	update();
}

void BitInspector::clear()
{
	begin = std::vector<SolutionPointData>::iterator();
	end = std::vector<SolutionPointData>::iterator();
	update();
}


void BitInspector::keyPressEvent(QKeyEvent* event)
{

}

void BitInspector::paintEvent(QPaintEvent* event)
{
	if (begin == end) {
		return;
	}
	int value = (parentWidget()->size().width() - FieldStart - EmptySpaceRight) / begin->bitVec.size();

	if (value > 1) {
		sizePerBit = value;
	}
	else {
		sizePerBit = 1;
	}
	//PaintList
	QPainter painter(this);
	painter.setRenderHint(QPainter::RenderHint::HighQualityAntialiasing);
	painter.setFont(QFont("Arial", 8));
	painter.setPen(QColor(0, 0, 0));
	QRect rect(event->rect());
	rect.moveTop(rect.top() - parentWidget()->size().height());
	int minimumY = rect.top() - parentWidget()->size().height();
	int maximumY = rect.bottom() + parentWidget()->size().height();

	QPen blackPen(QColor(0, 0, 0));
	QPen whitePen(QColor(255, 255, 255));
	int iteration = 0;
	int drawSize = (sizePerBit > 1)? sizePerBit - 1: 1;
	if (begin != end) {
		painter.fillRect(QRect(FieldStart -1 , 0, begin->bitVec.size() * sizePerBit, std::distance(begin, end) * ItemWidth), Qt::lightGray);
		int width = FieldStart + begin->bitVec.size() * sizePerBit;
		for (auto iter = begin; iter != end; iter++) {
			int startY = (iteration++) * ItemWidth + HeaderSpace;
			if (startY <= minimumY) continue;
			if (startY >= maximumY) break;
			QRect textRect(0, startY, 40, ItemWidth);
			painter.drawText(textRect, Qt::AlignCenter, QString::number(iter->iteration));
			textRect.moveLeft(40);
			painter.drawText(textRect, Qt::AlignCenter, QString::number(iter->particleNumber));
			textRect.moveLeft(80);
			textRect.setWidth(90);
			painter.drawText(textRect, Qt::AlignCenter, QString::number(iter->objectiveFunction, 'f', 1));
			//painter.setPen(blackPen);
			for (int j = 0; j < iter->bitVec.size(); j++) {
				painter.fillRect(FieldStart + j * sizePerBit, startY, drawSize, ItemWidth, (iter->bitVec[j]) ? blackPen.color() : whitePen.color());
			}
			painter.fillRect(FieldStart, startY, width - FieldStart, 1 ,QColor(140,140,140));
		}
	}
}
