#pragma once
#include "SearchSpacePlott.h"

class Bitfield : public SearchSpacePlott
{
	Q_OBJECT

public:
	unsigned int maxAmountOfSetBits = 5;
	Bitfield(QWidget *parent);
	~Bitfield();
	void frameGraphInView() override;
protected:
	void drawData(QPainter& painter) override;
	void searchForPointUnderCursor() override;
	void addPointsInWindowToScratchPad(VisibleWindow& window) override;
private:
	

};
