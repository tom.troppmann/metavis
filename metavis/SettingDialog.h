#pragma once
#include <QDialog>
#include <QSettings>
#include "ui_SettingDialog.h"
#include <QAbstractButton>

/**
 * A dialog to change the Setting for the main program.
 */
class SettingDialog : public QDialog
{
	Q_OBJECT

public:
	SettingDialog(QSettings* settings, QWidget *parent = Q_NULLPTR);
	~SettingDialog();

private:
	Ui::SettingDialog ui;
	QSettings* settings;
	/**
	 *  Apply the settings and close the SettingDialog afterwords.
	 */
	void acceptButtonClicked();
	/**
	 *  Save the changes from the dialog to the actual settings.
	 */
	void apply();
	


public slots:
	/**
	 * Handle the button clicked in the SettingDialog.
	 * @param button the Button returned from the Signal.
	 */
	void dialogButtonHandler(QAbstractButton* button);
};
