#pragma once

#include <QWidget>
#include <QPointF>
#include <QPainter>
#include <QMenu>



struct VisibleWindow {
	double xMin, xMax, yMin, yMax;
	VisibleWindow(double xMin, double xMax, double yMin, double yMax):
	xMin(xMin), xMax(xMax), yMin(yMin), yMax(yMax){}
	void moveUp(double percent);
	void moveDown(double percent);
	void moveLeft(double percent);
	void moveRight(double percent);
	void ZoomIn(double percent);
	void ZoomInX(double percent);
	void ZoomInY(double percent);
	void ZoomOut(double percent);
	void ZoomOutX(double percent);
	void ZoomOutY(double percent);
	double rangeX() const;
	double rangeY() const;
	bool inBoundX(QPointF point) const;
	bool inBoundY(QPointF point) const;
	bool inBound(QPointF point) const;
};

class Plott : public QWidget
{
	Q_OBJECT

public:
	//Grid
	bool enableGrid = false;

	Plott(QWidget *parent);
	~Plott();
	//Setter
	void setVisibleWindow(double xMin, double xMax, double yMin, double yMax);
	void setDefaultVisibleWindow(double xMin, double xMax, double yMin, double yMax);
	void setAxisLegend(QString xAxisLegend, QString yAxisLegend);
	//sets the window to the default window
	void virtual resetToDefaultWindow();

	QPointF transformGraphToView(QPointF graphpoint) const;
	QPointF transformGraphToView(QPointF graphpoint, double stregth_factorX, double stregth_factorY, QPointF translation) const;
	QPointF transformViewToGraph(QPointF viewpoint) const;
	QPointF transformViewToGraph(QPointF viewpoint, double stregth_factorX, double stregth_factorY, QPointF translation) const;
protected:
	//Represent the visual area in graph space that is shown in view space
	VisibleWindow window = VisibleWindow(0.0, 10, 0, 10);
	VisibleWindow defaultWindow = VisibleWindow(0.0, 10, 0, 10);



	virtual void drawData(QPainter& painter);
	//Returns the area in view space where 
	QRect getDisplayRect() const;
	
	//Display
	virtual void paintEvent(QPaintEvent* event) override;
	//Process user input events
	virtual void keyPressEvent(QKeyEvent* event) override;
	virtual void wheelEvent(QWheelEvent* event) override;
	virtual void mouseMoveEvent(QMouseEvent* event) override;
	virtual void mousePressEvent(QMouseEvent* event) override;
	virtual void mouseReleaseEvent(QMouseEvent* event) override;

	virtual void handleSelectedWindow(VisibleWindow& window, QMouseEvent* event);
	void setWindow(VisibleWindow& window);
private:
	
	//Axis Legends
	QString xAxisLegend = "X Legende", yAxisLegend = "Y Legend";
	//MouseControl
	bool isLeftMousePressed = false;
	bool isRightMousePressed = false;
	QPointF oldPositionForMouseEvent;
	QPointF mousePressedValue;
};
