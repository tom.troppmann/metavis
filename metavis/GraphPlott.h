#pragma once
#include <vector>

#include "Plott.h"
#include "RunData.h"
#include "GraphViewSettingDialog.h"
#include <QString>
#include <QLabel>
#include <string>
#include "Scratchpad.h"
#include "HoverButton.h"
#include "InformationPopUp.h"
class GraphViewSettingDialog;

struct GraphPlottSeries {
	std::vector<GraphDataPoint>* data;
	double xMin, xMax;
	double yMin, yMax;
	std::string runName;



	//Settings for visual
	QString description;
	QColor color;
	int lineWidth = 2;
	double circleRadius = 2.0;
	enum class SeriesType { Line, Dot, LineDot };
	bool useDataPointColor = false;
	SeriesType type;
	bool hide = false;
};

class GraphPlott : public Plott
{
	Q_OBJECT

public:

	GraphPlott(QWidget *parent,bool settingsButton = true, bool informationButton = true, bool gridButton = true);
	~GraphPlott();
	void addSeries(std::vector<GraphDataPoint>* line, std::string runName, QString description, QColor color, GraphPlottSeries::SeriesType type);
	void removeRunData(RunData* run);
	void removeAll();
	void setInformation(QString information);
	std::vector<GraphPlottSeries>& getSeriesVector();
	void virtual resetToDefaultWindow() override;
	void virtual frameGraphInView();
	void setDisplayLabel(QString file);
	void setScratchpad(Scratchpad* pad);
protected:
	QHBoxLayout* buttonPanel;
	void showInformation();
	void drawData(QPainter& painter) override;
	void mouseMoveEvent(QMouseEvent* event) override;
	std::vector<GraphPlottSeries> seriesVec;
	virtual void handleSelectedWindow(VisibleWindow& window, QMouseEvent* event) override;

	int stayStillTimeMS = 300;
	virtual void searchForPointUnderCursor();
	virtual void addPointsInWindowToScratchPad(VisibleWindow& window);
	//Help Function:
	double sqaredDistance(const QPointF& a, const QPointF& b);
	QPoint lastPosition;
	QTimer* timer = new QTimer(this);
	Scratchpad* pad = nullptr;
	HoverButton* informationButton = new HoverButton();
	InformationPopUp* popupWidget = new InformationPopUp(nullptr);
private:
	QMenu* selectMenu = new QMenu(this);
	QAction* displayAreaAction = new QAction("Display this area", this);
	QAction* scratchpadAction = new QAction("Add points to Scratchpad", this);
	GraphViewSettingDialog* dialog;
	QString displayNameOfDataThatIsDisplayed = "";
	QString informationButtonString = "No information.";
	QString graphplottInformation = "<h3><u>Control:</u></h3><b>Scrolling:</b> Zooms in and out.<br><b>Scrolling + CTRL:</b> Zooms only horizontally.<br><b>Scrolling + SHIFT:</b> Zooms only vertically.<br><b>Drag & Drop LMB:</b> Moves the visible area.<br><b>Drag & Drop RMB:</b> Selects area to display or add solutions to Scratchpad.<br><b>Drag & Drop RMB + CTRL:</b> Selects area to display.<br><b>Drag & Drop RMB + ALT:</b> Adds solutions to Scratchpad.<br><b>Arrow Keys:</b> Moves the visible area<br><b>Page Up:</b> Zooms in.<br><b>Page Down:</b> Zooms out.<br><b>CTRL + R:</b> Resets the visible area.<br>";
	QLabel* displayLabel;
	virtual void keyPressEvent(QKeyEvent* event) override;
};
