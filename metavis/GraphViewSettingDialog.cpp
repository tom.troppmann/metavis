#include "pch.h"
#include "GraphViewSettingDialog.h"
#include <QListWidgetItem>
#include <QPushButton>
#include "GraphViewSettingItem.h"
#include <QScrollArea>



GraphViewSettingDialog::GraphViewSettingDialog(GraphPlott* view, QString title)
	: QDialog(view, Qt::WindowSystemMenuHint | Qt::WindowTitleHint | Qt::WindowType::WindowCloseButtonHint), view(view)
{
	ui.setupUi(this);
	setWindowTitle(title);
	QWidget* toScroll = new QWidget(this);
	layout = new QVBoxLayout(this);
	layout->setContentsMargins(3, 0, 3, 0);
	toScroll->setLayout(layout);
	QVBoxLayout* myLayout = new QVBoxLayout(this);
	myLayout->setContentsMargins(0, 0, 0, 0);
	QScrollArea* scrollArea = new QScrollArea;
	scrollArea->setWidgetResizable(true);
	scrollArea->setWidget(toScroll);
	myLayout->addWidget(scrollArea);
	this->setLayout(myLayout);
}

GraphViewSettingDialog::~GraphViewSettingDialog()
{
}

void GraphViewSettingDialog::openDialog()
{
	//Generate Buttons
	//qDebug() << "vecsize " << vecsize << "     view->getSeriesVector().size() " << view->getSeriesVector().size();
	if (vecsize == view->getSeriesVector().size()) {
		this->activateWindow();
		this->setFocus();
		this->show();
		return;
	}
	else {
		vecsize = view->getSeriesVector().size();
		QLayoutItem* item;
		while ((item = layout->takeAt(0)))
			delete item;
	}

	for (auto iter = view->getSeriesVector().begin(); iter != view->getSeriesVector().end(); iter++) {
		GraphViewSettingItem* handle = new GraphViewSettingItem(view, &*iter, this);
		layout->addWidget(handle);
	}
	this->show();
}

