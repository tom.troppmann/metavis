#pragma once
#include <QColor>
namespace util {
	inline double linearInterpolate(double first, double second, double alpha) {
		return first * (1.0 - alpha) + second * alpha;
	}
	inline double inverseLinearInterpolation(double min, double max, double value) {
		if (max - min == 0) return max;
		else return (value - min) / (max - min);
	}
	inline QColor interpolateHSL(QColor& first, QColor& second, double alpha)
	{
		double firstH, firstS, firstL;
		double secondH, secondS, secondL;

		first.getHslF(&firstH, &firstS, &firstL);
		second.getHslF(&secondH, &secondS, &secondL);
		if (firstH == -1) {
			firstH = (secondH != -1) ? secondH : 0;
		}
		if (secondH == -1) {
			secondH = (firstH != -1) ? firstH : 0;
		}
		const double h = util::linearInterpolate(firstH, secondH, alpha);
		const double s = util::linearInterpolate(firstS, secondS, alpha);
		const double l = util::linearInterpolate(firstL, secondL, alpha);
		return QColor::fromHslF(h, s, l);
	}
	inline QColor interpolateRGB(QColor& first, QColor& second, double alpha)
	{
		const double r = util::linearInterpolate(first.redF(), second.redF(), alpha);
		const double g = util::linearInterpolate(first.greenF(), second.greenF(), alpha);
		const double b = util::linearInterpolate(first.blueF(), second.blueF(), alpha);
		return QColor::fromRgbF(r, g, b);
	}
}
