#pragma once
#include <thread>
#include <mutex>
#include <condition_variable>

class Concurrent
{
public:
	Concurrent();
	~Concurrent();
	void start();
	void pause();
	void reset();

protected:
	bool checkCancel();
	void checkPaused();


private:
	bool paused = false;
	bool cancel = false;
	std::thread* thread = nullptr;
	std::mutex mutex;
	std::condition_variable cv;
	void virtual run();
};

