#include "pch.h"
#include "InformationPopUp.h"

InformationPopUp::InformationPopUp(QWidget *parent)
	: QWidget(parent)
{
	this->setWindowFlag(Qt::Popup);
	QVBoxLayout* layout = new QVBoxLayout(this);
	setContentsMargins(3, 3, 3, 3);
	informationLabel->setTextFormat(Qt::RichText);
	layout->addWidget(informationLabel);
}

InformationPopUp::~InformationPopUp()
{
}

void InformationPopUp::setInformation(QString information)
{
	informationLabel->setText(information);
	update();
}

QString InformationPopUp::getInformation() const
{
	return informationLabel->text();
}
