#pragma once

#include <QWidget>
#include <QColor>
#include <QComboBox>
#include <list>
#include <QLineEdit>
#include "ColorButton.h"

struct ColorPoint {
	double alpha;
	QColor color;
	ColorPoint(double alpha, QColor color) : alpha(std::clamp(alpha, 0.0, 1.0)), color(color) {}
};


class ColorGradient : public QWidget
{
	Q_OBJECT

public:
	enum ColorInterpolationMode{ RGB, HSL};
	enum ColorMode{ Interpolate, Discrete};
	ColorGradient(QWidget *parent);
	~ColorGradient();
	QColor getColorFromAlpha(double alpha);
	QColor getColorFromValue(double value);
	void addColorPoint(double alpha);
	void removeColorPoint(ColorPoint& point);
	void setColorPointAlpha(ColorPoint& point, double newAlpha);
	void setColorPointColor(ColorPoint& point, QColor newColor);
	void setMode(ColorMode mode);
	void setInterpolationMode(ColorInterpolationMode mode);
	void setMinValue(double value);
	void setMaxValue(double value);
private:
	double minValue = 0, maxValue = 100;
	ColorMode mode = ColorMode::Interpolate;
	ColorInterpolationMode interpolationMode = ColorInterpolationMode::RGB;
	std::list<ColorPoint> pointList;
	//UI
	QLineEdit* minEdit;
	QLineEdit* maxEdit;
	ColorPoint* selected = nullptr;
	QComboBox* interpolationCombobox = nullptr;
	QComboBox* modeCombobox = nullptr;
	QSpacerItem* spacerH = nullptr;
	QLineEdit* alphaLineEdit = nullptr;
	ColorButton* colorButton = nullptr;
	QLineEdit* valueEdit;
	QLabel* colorLabel;
	QLabel* alphaLabel; 
	QLabel* valueLabel;
	void hideSelectionEdit(bool value);
	void setAlphaOfSelected();
	void setValueOfSelected();
	void selectColorPoint(ColorPoint* point);
	void paintEvent(QPaintEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;

	void minEditChangeEvent();
	void maxEditChangeEvent();


signals:
	void gradientChanged();
};
