#pragma once
#include <vector>
#include <string>
#include "RunData.h"


class Project
{
public:	
	void addFile(std::string fileName);
	void removeRunData(RunData* data);
	void removeLastData();
	void removeIndex(int index);
	void removeName(std::string fileName);
	void clearDataList();
	std::list<RunData>& getRunList();
private:
	std::list<RunData> runList;
};

