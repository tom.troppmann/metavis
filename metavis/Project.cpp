#include "pch.h"
#include "Project.h"

void Project::addFile(std::string metaLogFile)
{
	//If file exist and can be open
	runList.push_back(metaLogFile);
}

void Project::removeRunData(RunData* data)
{
	//Remove all series with this rundata
	auto iter = std::remove_if(runList.begin(), runList.end(), [data](RunData& run) {return (data == &run); });
	runList.erase(iter, runList.end());
}



void Project::removeLastData()
{
	if(!runList.empty()) runList.pop_back();
}

void Project::removeIndex(int index)
{
	//TODO 
}

void Project::removeName(std::string fileName)
{
	auto iter = std::remove_if(runList.begin(), runList.end(), [fileName](RunData& series) {return (fileName == series.name); });
	runList.erase(iter, runList.end());
}

void Project::clearDataList()
{	
	runList.clear();
}

std::list<RunData>& Project::getRunList()
{
	return runList;
}
