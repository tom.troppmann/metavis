#include "pch.h"
#include "Scratchpad.h"
#include "HoverButton.h"
#include <QAction>

Scratchpad::Scratchpad(QWidget *parent)
	: BitInspectorPanel(parent)
{
	HoverButton* rubbishButton = new HoverButton();
	rubbishButton->setMinimumSize(20, 20);
	rubbishButton->setMaximumSize(20, 20);
	rubbishButton->setIcon(QIcon(":/metavis/Resources/rubbish.svg"));
	rubbishButton->setHoveredIcon(QIcon(":/metavis/Resources/rubbish_hovered.svg"));
	rubbishButton->setAttribute(Qt::WA_TranslucentBackground);
	rubbishButton->setStyleSheet(rubbishButton->styleSheet() + "border: none;");
	rubbishButton->setToolTip("Clear scratchpad");
	qDebug() << topPanel->count();
	topPanel->insertWidget(topPanel->count()-1, rubbishButton);
	connect(rubbishButton, &QPushButton::released, this, &Scratchpad::clear);
	
	QAction* clearAction = new QAction("Clear");
	this->contextMenu->addAction(clearAction);
	connect(clearAction, &QAction::triggered, this, &Scratchpad::clear);

	popupWidget->setInformation("<h3><u>Scratchpad</u></h3> Shows selected solution from search space visualizations or metrics. Each solution gets displayed in a row. To add solutions go in a other visualization and press & drag the RMB(Right-Mouse-Button) to select the points to add to this scratchpad.<br><b>Clear button</b> removes all solutions from this pad.<br> Alternative is press RMB and \"Clear\" in the scratchpad. <br><b>Iteration:</b> Show the iteration the solution is generated.<br><b>Number:</b> Show which individual in the popullation is represent.<br><b>Objective Function:</b> Describes how well the solution is, it depends on the problem if a low or high value is desired.<br><b>Binary:</b> Shows the bitstring of the solution. White rectangle represents the bit is <i>false<i> and black <i>true<i>.");
}

Scratchpad::~Scratchpad()
{
}

void Scratchpad::addPoint(const SolutionPointData& point)
{
	inspector->clear();
	points.push_back(point);
	inspector->updateData(points.begin(), points.end());
}

void Scratchpad::clear()
{
	inspector->clear();
	points.clear();
}

void Scratchpad::contextMenuEvent(QContextMenuEvent* event)
{
	contextMenu->exec(event->globalPos());
}
