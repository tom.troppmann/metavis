#pragma once

#include <QWidget>
#include "ui_GraphViewSettingItem.h"
#include "GraphPlott.h"
#include "ColorButton.h"
class GraphPlott;

class GraphViewSettingItem : public QWidget
{
	Q_OBJECT

public:
	GraphViewSettingItem(GraphPlott* view, GraphPlottSeries* series, QWidget *parent = Q_NULLPTR);
	~GraphViewSettingItem();

private:
	Ui::GraphViewSettingItem ui;
	GraphPlottSeries* series;
	GraphPlott* view;
	ColorButton* colorButton;
	void dissableSlidersOnType();
	void setType(int type);
	void setColor(QColor color);
	void updateGroupBoxTitle();
};
