#pragma once

#include <QWidget>
#include "TsnePlott.h"
#include "RangeSlider.h"
#include "ColorGradient.h"
#include <QProgressBar>
#include "TsneSettings.h"
#include "tSneAlgo.h"
#include <string>
#include <vector>
#include "RunData.h"

class TsneControlPanel : public QWidget
{
	Q_OBJECT

public:
	TsneControlPanel(QWidget *parent);
	~TsneControlPanel();
	TsnePlott* plott = new TsnePlott(this);
	void assignData(std::vector<SolutionPointData>::iterator begin, std::vector<SolutionPointData>::iterator end, QString runName);
	void clear();


protected:
	RangeSlider* slider = new RangeSlider(this);
	ColorGradient* gradient = new ColorGradient(this);
	QProgressBar* progressBar = new QProgressBar();
	TsneSettings* tsneSettings = new TsneSettings(this);
	QLabel* iterationLabel = new QLabel();
	bool isOptionHidden = false;
	void toggleOptions();
	void resetPanel();

private:
	QString runName;
	std::vector<SolutionPointData>::iterator begin, end;
	tSneAlgo* algo = nullptr;
	QPushButton* showOptionsButton;
public slots:
	void start();
	void pause();
	void iterationChanged(int iter);
signals:
	void started();
	void finished();
};
