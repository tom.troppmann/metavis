#pragma once
#include <QObject>
#include "RunData.h"
#include "Concurrent.h"
#include <vector>


class tSneAlgo : public QObject, public Concurrent
{
	Q_OBJECT
public:
	tSneAlgo(std::vector<SolutionPointData>::iterator begin, std::vector<SolutionPointData>::iterator end, double** YnotInitialized, double perplexity, double learningRate, int maxIter);
	~tSneAlgo();
	int actualIteration = 0;
	double perplexity, learningRate;
	// in between 0.0 and 1.0
	double theta = 0;
	int maxIter = 750, stopLyingIter = 120, momentumSwitchIter = 20;
private:
	int N, D, outputDimesion = 2;
	bool useRandomSeed = true;
	bool skipRandomInit = false;
	int randomSeet = 182437123;

	//intermediate
	double* inputArrayX;
	double* outputArrayY;
	void run() override;

public:
	void setLearningRate(double epsillon);
	void setPerplexity(double perplexity);
signals:
	void changedIter(int iter);
	void algoDone();
};

