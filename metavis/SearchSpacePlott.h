#pragma once

#include <QWidget>
#include <QMouseEvent>
#include "GraphPlott.h"


class SearchSpacePlott : public GraphPlott
{
	Q_OBJECT

public:
	SearchSpacePlott(QWidget *parent);
	~SearchSpacePlott();
protected:
	int minIter = 0;
	int maxIter = 100;

	int circleSize = 2;
	double transparentAlphaValue = 1;
	virtual void mouseMoveEvent(QMouseEvent* event) override;
public slots:
	void setMinimumIterationToDispaly(int min);
	void setMaximumIterationToDispaly(int max);
};
