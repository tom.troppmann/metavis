#include "pch.h"
#include "SettingDialog.h"
#include <QDebug>
SettingDialog::SettingDialog(QSettings* settings, QWidget* parent)
	: QDialog(parent, Qt::WindowTitleHint | Qt::WindowCloseButtonHint), settings(settings)
{
	ui.setupUi(this);
	//manuel connect all button with buttonHandler
	connect(ui.buttonBox, SIGNAL(clicked(QAbstractButton*)), SLOT(dialogButtonHandler(QAbstractButton*)));
}

SettingDialog::~SettingDialog()
{
}



void SettingDialog::acceptButtonClicked()
{
	qDebug() << "Accept";
	apply();
	accept();
}

void SettingDialog::apply()
{
	qDebug() << "Apply";
}


void SettingDialog::dialogButtonHandler(QAbstractButton* button)
{
	switch (ui.buttonBox->buttonRole(button)) {
		case QDialogButtonBox::ButtonRole::AcceptRole:
			acceptButtonClicked();
			break;
		case QDialogButtonBox::ButtonRole::ApplyRole:
			apply();
			break;
		case QDialogButtonBox::ButtonRole::RejectRole:
		default:
			/* close the window and dont apply changes*/
			reject();
			break;
	}
}



