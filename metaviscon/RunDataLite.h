#pragma once
#include <vector>
#include <string>
#include <fstream>

struct SolutionPointData {
	int iteration;
	double objectiveFunction;
	int particleNumber;
	std::vector<bool> bitVec;
};


class RunDataLite
{
public:
	std::string name;
	std::string information;
	std::string filePath;
	std::vector<SolutionPointData> solutionVec;
	RunDataLite(std::string filePath);
	bool badFile();
private:
	bool badFileFlag = false;
	std::fstream fileStream;
	int actualLine = 0;
	void getLine(std::string& bufferString);
};

