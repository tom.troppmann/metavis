#include <fstream>
#include <string>
#include <iostream>
#include <random>





int main18236() {
	std::cout << "Hello World";
	std::fstream writer("generated.metalog", std::ios::out);
	const int numberOfParts = 5;
	const int amountOfBits = 100;
	const int pointsPerPart = 20;

	//random
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	std::uniform_real_distribution<double> doubleDistr(0.0, 1.0);


	for (int i = 0; i < numberOfParts; i++) {
		for (int pN = 0; pN < pointsPerPart; pN++) {
			writer << "of:" << std::to_string(10.0 * (i + 1)) << " i:" << i << " pN:" << pN << " b:";
			int part = amountOfBits / numberOfParts;
			for (int index = 0; index < amountOfBits; index++) {
				if (i * part < index && index <= (i+1)*part) {
					writer << (doubleDistr(gen) < 0.95 ?"1":"0");
				}
				else {
					writer << "0";
				}
			}
			writer << std::endl;

		}
	}
	writer.close();
	return 0;
}